package com.flnk.pocketfit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;


import com.flnk.pocketfit.database.DBHandler;

import com.flnk.pocketfit.ui.login.LoginActivity;

public class MainActivity extends AppCompatActivity {






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        // delete db at launch
        this.deleteDatabase("User.db");
    }


    public void goToLoginAc(View view) {
        Intent intent = new Intent (this, LoginActivity.class);
        startActivity(intent);
    }




}
