package com.flnk.pocketfit;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.flnk.pocketfit.database.DBHandler;


public class Profile extends AppCompatActivity implements  AdapterView.OnItemSelectedListener {



    EditText etname, etage, etheight, etweight;
    DBHandler dbHandler;
    public String spintext;
    public String spin2text;
    Spinner spinner;


 @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_profile);


        //spinner levels

        Spinner spinner = findViewById(R.id.level);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.levelsArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);


        //spinner gender

        Spinner spinner2 = findViewById(R.id.gender);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.genderArray, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter1);
        spinner2.setOnItemSelectedListener(this);





        //DB
        etname = findViewById(R.id.name);
        etage = findViewById(R.id.age);
        etheight = findViewById(R.id.height);
        etweight = findViewById(R.id.weight);


        dbHandler = new DBHandler(this);


    }




    //adapter 1 and 2 with switch

    public void onItemSelected(AdapterView<?>arg0, View view, int i, long arg2) {

            switch (arg0.getId()){
                case R.id.level:
                    Spinner spinner = (Spinner)findViewById(R.id.level);
                    spintext = spinner.getSelectedItem().toString();
                        break;
                case R.id.gender:
                    Spinner spinner2 = (Spinner)findViewById(R.id.gender);
                    spin2text = spinner2.getSelectedItem().toString();

            }


    }

    public void onNothingSelected(AdapterView<?> parent) {

    }











    //////////////button to 0000save + database updates
        public void buttonSave (View view){
            Intent goal = new Intent(this, Goals.class);
            startActivity(goal);
            String names = etname.getText().toString();
            int ages = Integer.parseInt(etage.getText().toString());
            int heights = Integer.parseInt(etheight.getText().toString());
            String gender = spin2text;
            int weights = Integer.parseInt(etweight.getText().toString());
            String level = spintext;




            boolean status = dbHandler.addUser(names, ages, heights,gender, weights,level, null);


            if (status)
                Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "fail", Toast.LENGTH_SHORT).show();

        }




}








