package com.flnk.pocketfit;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;

import com.flnk.pocketfit.database.DBHandler;

import java.util.ArrayList;


public class Dashboardshred extends AppCompatActivity {

    DBHandler dbHandler;
    public double bmr = 0;
    public int mildlose = 0;
    public double activityfac= 0;
    public int carbsG = 0;
    public int fatG = 0;
    public int proteinG = 0;



    ListView listViewAge;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_dashshred);






                                             //converting database
        // .join requires api 26+

                    //this gets NAME
        dbHandler = new DBHandler(this);
        ArrayList<String> list1 = dbHandler.getName();
        String Namelist = String.join(", ", list1);

                //this gets AGE from database

       // listViewAge = findViewById(R.id.listVeiw);
       ArrayList<String> list2 = dbHandler.getAge();
        //ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.list_item,list1);
        String Agelist = String.join(", ", list2);

                //get HEIGHT DATA

        ArrayList<String> list3 = dbHandler.getHeight();
        String Heightlist = String.join(", ", list3);

        //get gender DATA

        ArrayList<String> list4 = dbHandler.getGender();
        String Genderlist = String.join(", ", list4);

        //get HEIGHT DATA
        ArrayList<String> list5 = dbHandler.getWeight();
        String Weightlist = String.join(", ", list5);

        //get LEVEL DATA
        ArrayList<String> list6 = dbHandler.getLevel();
        String Levellist = String.join(", ", list6);







        // creating activity factor based on selection

        if (Levellist.equals("Sedentary: Little or no exercise")) {
               activityfac = 1.2;
        }
        else if (Levellist.equals("Light: 1-3 times a week")){
             activityfac = 1.375;
        }
        else if (Levellist.equals("Moderate: 4-5 times a week")) {
             activityfac = 1.55;
        }
        else if (Levellist.equals("Active: Daily or intense 3-4 times a week")){
             activityfac = 1.725;
        }
        else if (Levellist.equals("Very Active: Intense 6-7 times a week")){
             activityfac = 1.9;
        }
//
//
//
//        //formula for calculating BMR men , cast to int
//
        if (Genderlist.equals("Male")) {

           bmr = ((10 * Integer.parseInt(Weightlist)) + (6.25 * Integer.parseInt(Heightlist)) - (5 * Integer.parseInt(Agelist)) + 5);

        }
        else if (Genderlist.equals("Female")){
            bmr = ((10 * Integer.parseInt(Weightlist)) + (6.25 * Integer.parseInt(Heightlist)) - (5 * Integer.parseInt(Agelist)) - 161);
        }
//
//

        //midlose pre = 80% off maintence call // weightloss

        double mildlosepre = (0.8) * (activityfac*bmr);
        mildlose = (int) mildlosepre;



        //PROTIEN

        //grams
        proteinG = (int) 2.2 * Integer.parseInt(Weightlist) ;
        //cal
        int proteinC = proteinG * 4;



        //FAT

        //cals
        int fatC = (int) (0.3 * mildlose);
        //grams
        fatG = fatC / 9;


        //CARBS
        int carbsC = mildlose - (proteinC + fatC);
        carbsG = (int) carbsC / 4;
//
//
//
//      calories value
//      setting text to int midlose number , id from layout, value from number here

        TextView calview = (TextView) this.findViewById(R.id.calnumber);
        calview.setText(String.valueOf(mildlose) + " Daily Calories");


        //workout box

        // setting mame of user to name text

        TextView username = (TextView) this.findViewById(R.id.nameView);
        username.setText("Welcome " + String.valueOf(Namelist));

        //age set

        TextView yourage = this.findViewById(R.id.ageView);
        yourage.setText(String.valueOf(Agelist));

        //weight
        TextView yourweight = this.findViewById(R.id.weightView);
        yourweight.setText(String.valueOf(Weightlist) + "kg");

        //height

        TextView yourheight = this.findViewById(R.id.heightView);
        yourheight.setText(String.valueOf(Heightlist) + "cm");





        // values box


        //setting carbs to text view

        TextView carbstext = (TextView) this.findViewById(R.id.carbsView);
        carbstext.setText(String.valueOf(carbsG));

        //setting protein to ptext
        TextView proteintext = (TextView) this.findViewById(R.id.proteinView);
        proteintext.setText(String.valueOf(proteinG));

        //setting fat to fattext
        TextView fattext = (TextView) this.findViewById(R.id.fatView);
        fattext.setText(String.valueOf(fatG));











        //debug
//        Log.d("myTag1", Namelist);
//        Log.d("myTag2", Agelist);
//        Log.d("myTag3", Genderlist);
//        Log.d("myTag4", Levellist);
//        Log.d("myTag5", String.valueOf(activityfac));
//        Log.d("myTag6", String.valueOf(bmr));
//        Log.d("myTag6", String.valueOf(mildlose));






    }




    public void buttonsatDash (View view) {
//
//        //        button with switch for views!
//         use count to

        switch (view.getId()) {

            case R.id.youButton:
                //code
                Intent youpage = new Intent(this, YouShred.class);
                startActivity(youpage);
                //count = 1;

                break;

            case R.id.dashButton:
                //
                Intent dashpage = new Intent(this, Dashboardshred.class);
                startActivity(dashpage);
                //count = 2;
                break;

            case R.id.planButton:
                Intent planpage = new Intent(this,PlanShred.class);
                startActivity(planpage);
                break;

            case R.id.setButton:
                Intent setttingspage = new Intent(this, SettingsShred.class);
                startActivity(setttingspage);
                break;
        }

    }
}
















