package com.flnk.pocketfit;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.flnk.pocketfit.database.DBHandler;

import java.util.ArrayList;


public class YouShred extends AppCompatActivity {

    DBHandler dbHandler;
    EditText  etnameshred, etageshred, etheightshred, etweightshred;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_youshred);






        dbHandler = new DBHandler(this);


        //setting values of text to user

        //NAME
        ArrayList<String> list1 = dbHandler.getName();
        String Namelist = String.join(", ", list1);
        TextView username = (TextView) this.findViewById(R.id.nameview);
        username.setText(String.valueOf(Namelist));

        //AGE
        ArrayList<String> list2 = dbHandler.getAge();
        String Agelist = String.join(", ", list2);
        TextView yourage = this.findViewById(R.id.ageview);
        yourage.setText(String.valueOf(Agelist));

        //HEIGHT
        ArrayList<String> list3 = dbHandler.getHeight();
        String Heightlist = String.join(", ", list3);
        TextView yourheight = this.findViewById(R.id.heightview);
        yourheight.setText(String.valueOf(Heightlist));

        //WEGIHTR
        ArrayList<String> list5 = dbHandler.getWeight();
        String Weightlist = String.join(", ", list5);
        TextView yourweight = this.findViewById(R.id.weightview);
        yourweight.setText(String.valueOf(Weightlist));






        //lock string to text boxes for pull
        etnameshred = findViewById(R.id.nameview);
        etageshred = findViewById(R.id.ageview);
        etheightshred = findViewById(R.id.heightview);
        etweightshred = findViewById(R.id.weightview);

    }

    public void buttonSave (View view){

        String name = etnameshred.getText().toString();
        int ages = Integer.parseInt(etageshred.getText().toString());
        int heights = Integer.parseInt(etheightshred.getText().toString());

        int weights = Integer.parseInt(etweightshred.getText().toString());





        boolean status = dbHandler.updateData(name, ages, heights, weights);


        if (status)
            Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "failed, update all values", Toast.LENGTH_SHORT).show();

        Intent refresh = new Intent(this, Dashboardshred.class);
        startActivity(refresh);
        this.finish();

    }
}