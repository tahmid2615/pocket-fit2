package com.flnk.pocketfit.database;

import android.provider.BaseColumns;

public class User {
    public User() {
    }

    public static final class UserDetails implements BaseColumns{

        public static final String TABLE_NAME = "user";
        public static final String COL_NAME = "name";
        public static final String COL_AGE = "age";
        public static final String COL_HEIGHT = "height";
        public static final String COL_GENDER = "gender";
        public static final String COL_WEIGHT = "weight";
        public static final String COL_LEVEL = "level";
        public static final String COL_GOAL = "goal";

    }
}
