package com.flnk.pocketfit.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.widget.Button;

import java.util.ArrayList;


public class DBHandler extends SQLiteOpenHelper {

    public static final String DB_NAME = "User.db";
    public static final int DB_VERSION = 1;
    Button buttonUpdate;


    public DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String USER_TABLE = "CREATE TABLE "+ User.UserDetails.TABLE_NAME+" ("+
                User.UserDetails.COL_NAME+" TEXT,"+
                User.UserDetails.COL_AGE+" NUMBER,"+
                User.UserDetails.COL_HEIGHT+" NUMBER,"+
                User.UserDetails.COL_GENDER+" TEXT,"+
                User.UserDetails.COL_WEIGHT+" NUMBER,"+
                User.UserDetails.COL_LEVEL+" TEXT,"+
                User.UserDetails.COL_GOAL+ " TEXT )";

        db.execSQL(USER_TABLE);

    }



    ///////////////////////////////////////////////////////user!!
    public boolean addUser(String name, Integer age, Integer height, String gender, Integer weight, String level, String goal){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(User.UserDetails.COL_NAME,name);
        values.put(User.UserDetails.COL_AGE,age);
        values.put(User.UserDetails.COL_HEIGHT,height);
        values.put(User.UserDetails.COL_GENDER,gender);
        values.put(User.UserDetails.COL_WEIGHT,weight);
        values.put(User.UserDetails.COL_LEVEL,level);
        values.put(User.UserDetails.COL_GOAL,goal);

        long id = db.insert(User.UserDetails.TABLE_NAME,null,values);

        if (id>0)
            return true;
        else
            return false;

    }

    //updating data on YOUSHRED

    public boolean updateData (String name, Integer age, Integer height, Integer weight){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(User.UserDetails.COL_AGE,age);
        values.put(User.UserDetails.COL_HEIGHT,height);
        values.put(User.UserDetails.COL_WEIGHT,weight);

        long id = db.update(User.UserDetails.TABLE_NAME,values, User.UserDetails.COL_NAME+" = ? ", new String[]{name});
         if (id>0)
             return true;
         else
             return false;
        }

    



                            // GET NAME DATA
    public ArrayList<String> getName (){
        ArrayList<String> list1 = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query (User.UserDetails.TABLE_NAME,
                new String[] {User.UserDetails.COL_NAME},
                null, null, null, null, null);

        if (cursor.moveToNext()){
            list1.add(cursor.getString(cursor.getColumnIndex(User.UserDetails.COL_NAME)));
        }
        return list1;
    }


                                                 //GET AGE DATA

    public ArrayList<String>  getAge (){
        ArrayList<String> list2 = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(User.UserDetails.TABLE_NAME,
                new String[] {User.UserDetails.COL_AGE},
                null, null, null, null, null);

        if (cursor.moveToNext()){
            list2.add(cursor.getString(cursor.getColumnIndex(User.UserDetails.COL_AGE)));
        }
        return list2;
    }


                        //GET HEIGHT DATA

    public ArrayList<String>  getHeight (){
        ArrayList<String> list3 = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(User.UserDetails.TABLE_NAME,
                new String[] {User.UserDetails.COL_HEIGHT},
                null, null, null, null, null);

        if (cursor.moveToNext()){
            list3.add(cursor.getString(cursor.getColumnIndex(User.UserDetails.COL_HEIGHT)));
        }
        return list3;
    }


            // GET GENDER DATA


    public ArrayList<String>  getGender (){
        ArrayList<String> list4 = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.query(User.UserDetails.TABLE_NAME,
                new String[] {User.UserDetails.COL_GENDER},
                null, null, null, null, null);


        if (cursor.moveToNext()){
            list4.add(cursor.getString(cursor.getColumnIndex(User.UserDetails.COL_GENDER)));
        }
        return list4;
    }

    // GET WEight DATA

    public ArrayList<String>  getWeight (){
        ArrayList<String> list5 = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.query(User.UserDetails.TABLE_NAME,
                new String[] {User.UserDetails.COL_WEIGHT},
                null, null, null, null, null);


        if (cursor.moveToNext()){
            list5.add(cursor.getString(cursor.getColumnIndex(User.UserDetails.COL_WEIGHT)));
        }
        return list5;
    }

    // GET LEVEL DATA


    public ArrayList<String>  getLevel (){
        ArrayList<String> list5 = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.query(User.UserDetails.TABLE_NAME,
                new String[] {User.UserDetails.COL_LEVEL},
                null, null, null, null, null);


        if (cursor.moveToNext()){
            list5.add(cursor.getString(cursor.getColumnIndex(User.UserDetails.COL_LEVEL)));
        }
        return list5;
    }




    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



    public void del() {
        //Delete all records of table
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + User.UserDetails.TABLE_NAME);
        db.close();
    }


}
